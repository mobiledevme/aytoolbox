//
//  ConcurrentOperation.swift
//  Created by Austin Younts on 8/23/15.
//

import Foundation

open class ConcurrentOperation: Operation {

    override open var isAsynchronous: Bool {
        return true
    }

    @objc class func keyPathsForValuesAffectingIsReady() -> Set<NSObject> {
        return ["state" as NSObject]
    }

    @objc class func keyPathsForValuesAffectingIsExecuting() -> Set<NSObject> {
        return ["state" as NSObject]
    }

    @objc class func keyPathsForValuesAffectingIsFinished() -> Set<NSObject> {
        return ["state" as NSObject]
    }

    @objc class func keyPathsForValuesAffectingIsCancelled() -> Set<NSObject> {
        return ["state" as NSObject]
    }


    private enum State: Int {
        /// The initial state of an `Operation`.
        case initialized

        /// The `Operation` is executing.
        case executing

        /// The `Operation` has finished executing.
        case finished

        /// The `Operation` has been cancelled.
        case cancelled
    }

    private var _state = State.initialized

    private var state: State {
        get {
            return _state
        }

        set(newState) {
            // Manually fire the KVO notifications for state change, since this is "private".

            willChangeValue(forKey: "state")

            switch (_state, newState) {
            case (.finished, _):
            break // cannot leave the finished state
            default:
                _state = newState
            }

            didChangeValue(forKey: "state")
        }
    }

    override open var isExecuting: Bool {
        return state == .executing
    }

    override open var isFinished: Bool {
        return state == .finished
    }

    override open var isCancelled: Bool {
        return state == .cancelled
    }

    override final public func start() {

        if !(state == .cancelled) {
            state = .executing
        }

        execute()
    }

    open func execute() {
        print("\(type(of: self)) must override `execute()`.", terminator: "")

        finish()
    }

    override open func cancel() {
        state = .cancelled
    }

    public final func finish() {
        state = .finished
    }
}

