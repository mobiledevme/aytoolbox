//
//  AutoLayoutHelpers.swift
//  Created by Austin on 5/6/17.
//

import UIKit

@available(iOS 9.0, *)
public extension UIViewController {

    func addAndPin(_ viewController: UIViewController, to container: UIView? = nil) {
        let containerView = container ?? view
        addChild(viewController)
        containerView?.addSubview(viewController.view)
        containerView?.pin(view: viewController.view)
        viewController.didMove(toParent: self)
    }

    func addChild(_ viewController: UIViewController, toStackView stackView: UIStackView) {
        addChild(viewController)
        stackView.addArrangedSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
}

@available(iOS 9.0, *)
public extension UIView {

    func useAutolayout() -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        return self
    }

    convenience init(usingAutolayout: Bool) {
        self.init()
        translatesAutoresizingMaskIntoConstraints = !usingAutolayout
    }

    static func flexibleView() -> UIView {
        let v = UIView().useAutolayout()
        v.backgroundColor = .clear
        v.setContentHuggingPriority(UILayoutPriority(rawValue: 1), for: .vertical)
        v.setContentHuggingPriority(UILayoutPriority(rawValue: 1), for: .horizontal)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1), for: .vertical)
        v.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1), for: .horizontal)
        return v
    }

    static func spacerView(height: CGFloat) -> UIView {
        let v = UIView().useAutolayout()
        v.backgroundColor = .clear
        v.heightAnchor.constraint(equalToConstant: height).isActive = true
        return v
    }

    static func spacerView(width: CGFloat) -> UIView {
        let v = UIView().useAutolayout()
        v.backgroundColor = .clear
        v.heightAnchor.constraint(equalToConstant: width).isActive = true
        return v
    }

    func pin(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    func updateLayout(animated: Bool = true, duration: TimeInterval = 0.25) {
        if animated { UIView.animate(withDuration: duration) { self.layoutIfNeeded() } }
        else { layoutIfNeeded() }
    }
}
