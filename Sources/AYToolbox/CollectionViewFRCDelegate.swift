//
//  CollectionViewFRCDelegate.swift
//

import UIKit
import CoreData

private typealias SectionChange = (changeType: NSFetchedResultsChangeType, sectionIndex: Int)
private typealias ItemChange = (changeType: NSFetchedResultsChangeType, indexPath: IndexPath?, newIndexPath: IndexPath?)

public class CollectionViewFRCDelegate: NSObject, NSFetchedResultsControllerDelegate {

    public typealias ContentChangedAction = () -> Void

    private let collectionView: UICollectionView

    private var sectionChanges = [SectionChange]()
    private var itemChanges = [ItemChange]()
    private var updatedObjects = [IndexPath : AnyObject]()

    public var contentChangedAction: ContentChangedAction?

    public init(collectionView: UICollectionView) {
        self.collectionView = collectionView
    }

    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        sectionChanges.removeAll()
        itemChanges.removeAll()
        updatedObjects.removeAll()
    }

    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        let updates = {
            self.applySectionChanges()
            self.applyItemChanges()
        }
        guard !sectionChanges.isEmpty || !itemChanges.isEmpty else {
            return
        }
        collectionView.performBatchUpdates(updates, completion: nil)
        contentChangedAction?()
    }

    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {

        sectionChanges.append((changeType: type, sectionIndex: sectionIndex))
    }

    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        itemChanges.append((changeType: type, indexPath: indexPath, newIndexPath: newIndexPath))
    }

    private func applySectionChanges() {
        for (changeType, sectionIndex) in sectionChanges {
            let section = IndexSet(integer: sectionIndex)

            switch changeType {
            case .insert:
                collectionView.insertSections(section)
            case .delete:
                collectionView.deleteSections(section)
            default:
                break
            }
        }
    }

    private func applyItemChanges() {
        for (changeType, indexPath, newIndexPath) in itemChanges {
            switch changeType {
            case .insert:
                collectionView.insertItems(at: [newIndexPath!])
            case .delete:
                collectionView.deleteItems(at: [indexPath!])
            case .update:
                collectionView.reloadItems(at: [indexPath!])
            case .move:
                collectionView.deleteItems(at: [indexPath!])
                collectionView.insertItems(at: [newIndexPath!])
            @unknown default:
                break
            }
        }
    }
}

