//
//  KeyboardHandler.swift
//  Created by Austin Younts on 5/29/17.
//

import UIKit

@available(iOS 9.0, *)
public extension UIView {

    var keyboardLayoutGuide: UILayoutGuide {
        let handler = KeyboardHandler.shared

        if let guide = handler.guide(for: self) {
            return guide.guide
        }

        let guide = createKeyboardGuide()

        handler.add(guide: guide)

        return guide.guide
    }

    fileprivate func createKeyboardGuide() -> KeyboardGuide {

        guard let window = window else {
            fatalError("Keyboard Layout Guide cannot be used until a view is attached to a window.")
        }

        let g = UILayoutGuide()
        addLayoutGuide(g)
        g.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
        g.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
        g.trailingAnchor.constraint(equalTo: window.trailingAnchor).isActive = true

        let height = g.heightAnchor.constraint(equalToConstant: 0)
        height.isActive = true

        return KeyboardGuide(guide: g, height: height)
    }
}

@available(iOS 9.0, *)
private class KeyboardHandler: NSObject {

    static let shared: KeyboardHandler = .init()

    private var layoutGuides: [KeyboardGuide] = []

    override private init() {
        super.init()
        registerKeyboardHandlers()
    }

    private func registerKeyboardHandlers() {

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(handleKeyboardNotification(_:)),
                       name: UIResponder.keyboardWillShowNotification, object: nil)
        nc.addObserver(self, selector: #selector(handleKeyboardNotification(_:)),
                       name: UIResponder.keyboardWillHideNotification, object: nil)
        nc.addObserver(self, selector: #selector(handleKeyboardNotification(_:)),
                       name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    @objc private func handleKeyboardNotification(_ note: Notification) {
        cleanupLayoutGuides()
        let info = KeyboardInfo(notification: note)
        for guide in layoutGuides {
            guide.update(withInfo: info)
        }
    }

    private func cleanupLayoutGuides() {

        var tempGuides = layoutGuides

        for guide in layoutGuides {
            guard guide.guide.owningView == nil else {
                continue
            }
            if let index = tempGuides.firstIndex(of: guide) {
                tempGuides.remove(at: index)
            }
        }

        layoutGuides = tempGuides
    }

    func guide(for view: UIView) -> KeyboardGuide? {
        cleanupLayoutGuides()
        return layoutGuides.filter { $0.guide.owningView == view }.first
    }

    func add(guide: KeyboardGuide) {
        layoutGuides.append(guide)
    }
}

@available(iOS 9.0, *)
private func ==(lhs: KeyboardGuide, rhs: KeyboardGuide) -> Bool {
    return lhs.guide == rhs.guide
}

@available(iOS 9.0, *)
private struct KeyboardGuide: Equatable {
    let guide: UILayoutGuide
    let height: NSLayoutConstraint

    func update(withInfo info: KeyboardInfo) {

        guard
            let view = guide.owningView,
            let window = view.window else {
            return
        }

        let keyboardHeight = window.frame.height - info.endingFrame.minY
        // Update layout guide height
        height.constant = keyboardHeight
        // Update view layout
        view.setNeedsLayout()

        let animations = {
            view.layoutIfNeeded()
        }

        UIView.animate(withDuration: info.duration, delay: 0,
                       options: [info.curve.options],
                       animations: animations, completion: nil)
    }
}

private extension UIView.AnimationCurve {
    var options: UIView.AnimationOptions {
        switch self {
        case .easeIn:
            return .curveEaseIn
        case .easeOut:
            return .curveEaseOut
        case .easeInOut:
            return .curveEaseInOut
        case .linear:
            return .curveLinear
        @unknown default:
            return .curveEaseInOut
        }
    }
}

@available(iOS 9.0, *)
private struct KeyboardInfo {
    let beginningFrame: CGRect
    let endingFrame: CGRect
    let duration: TimeInterval
    let curve: UIView.AnimationCurve
    let isLocal: Bool

    init(notification note: Notification) {
        self.beginningFrame = note.beginningFrame
        self.endingFrame = note.endingFrame
        self.duration = note.duration
        self.curve = note.curve
        self.isLocal = note.isLocal
    }
}

@available(iOS 9.0, *)
private extension Notification {

    var beginningFrame: CGRect {
        guard let value = userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return .zero
        }
        return value.cgRectValue
    }
    var endingFrame: CGRect {
        guard let value = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return .zero
        }
        return value.cgRectValue
    }
    var duration: TimeInterval {
        guard let value = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber else {
            return 0
        }
        return value.doubleValue
    }
    var curve: UIView.AnimationCurve {
        guard let value = userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber else {
            return .easeInOut
        }
        return UIView.AnimationCurve(rawValue: value.intValue) ?? .easeInOut
    }
    var isLocal: Bool {
        guard let value = userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? NSNumber else {
            return false
        }
        return value.boolValue
    }
}
