//
//  NetworkOperation.swift
//  BlackRock
//
//  Created by Austin Younts on 1/17/18.
//  Copyright © 2018 AB. All rights reserved.
//

import Foundation

public protocol NetworkOperationDelegate: class {
    func operation<T>(_ operation: NetworkOperation, didParse values: T) where T: Codable
}

open class NetworkOperation: ConcurrentOperation {

    public typealias DataTaskValues = (Data?, URLResponse?, Error?)
    public typealias DataTaskCompletion = (DataTaskValues) -> ()
    public typealias Completion = () -> Void

    public let session: URLSession
    private let operationCompletion: Completion?

    public weak var delegate: NetworkOperationDelegate?

    public init(session: URLSession, completion: Completion? = nil) {
        self.session = session
        self.operationCompletion = completion
        super.init()
    }

    open func handleTaskCompletion(_ completion: DataTaskValues) {
        DispatchQueue.main.async {
            self.operationCompletion?()
        }
        finish()
    }

    open func check<T>(_ completion: DataTaskValues, for request: AnyRequestable<T>) -> Result<T> {
        let result = Result<Data>(value: completion.0, error: completion.2)
        switch result {
        case .failure(let error):
            return Result<T>.failure(error)
        case .success(let data):
            do {
                let decoder = JSONDecoder()
                if let decodingStrategy = request.dateDecodingStrategy {
                    decoder.dateDecodingStrategy = decodingStrategy
                }
                let decoded = try decoder.decode(type(of: request).Payload.self, from: data)
                DispatchQueue.main.async {
                    self.delegate?.operation(self, didParse: decoded)
                }
                return Result.success(decoded)
            }
            catch let error {
                return Result<T>.failure(error)
            }
        }
    }

}
