//
//  TableViewFRCDelegate.swift
//  AYUtilities
//
//  Created by Austin on 2/10/18.
//  Copyright © 2018 ARY. All rights reserved.
//

import UIKit
import CoreData

private typealias SectionChange = (changeType: NSFetchedResultsChangeType, sectionIndex: Int)
private typealias ItemChange = (changeType: NSFetchedResultsChangeType, indexPath: IndexPath?, newIndexPath: IndexPath?)

public class TableViewFRCDelegate: NSObject, NSFetchedResultsControllerDelegate {

    public typealias ContentChangedAction = () -> Void

    private let tableView: UITableView

    private var sectionChanges = [SectionChange]()
    private var itemChanges = [ItemChange]()
    private var updatedObjects = [IndexPath : AnyObject]()

    public var contentChangedAction: ContentChangedAction?

    public init(tableView: UITableView) {
        self.tableView = tableView
    }

    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        contentChangedAction?()
    }

    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange sectionInfo: NSFetchedResultsSectionInfo,
                           atSectionIndex sectionIndex: Int,
                           for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            break
        }
    }

    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange anObject: Any,
                           at indexPath: IndexPath?,
                           for type: NSFetchedResultsChangeType,
                           newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        @unknown default:
            break
        }
    }
}
