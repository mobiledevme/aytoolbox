//
//  NetworkHelpers.swift
//  BlackRock
//
//  Created by Austin Younts on 1/17/18.
//  Copyright © 2018 AB. All rights reserved.
//

import Foundation

/// Defines the values needed to create a URLRequest.
public protocol URLRequestable {

    associatedtype Payload: Codable

    var method: HTTPMethod { get }
    var host: URLHost { get }
    var path: URLPath { get }
    var scheme: URLScheme { get }

    var parameterType: ParameterEncodingType { get }
    var queryParameters: [URLQueryItem]? { get }

    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy? { get }

    var erased: AnyRequestable<Payload> { get }
}

public extension URLRequestable {
    var erased: AnyRequestable<Payload> {
        return AnyRequestable<Payload>(method: method,
                                       host: host,
                                       path: path,
                                       scheme: scheme,
                                       parameterType: parameterType,
                                       queryParameters: queryParameters,
                                       dateDecodingStrategy: dateDecodingStrategy)
    }
}

public struct AnyRequestable<T: Codable>: URLRequestable {

    public typealias Payload = T

    public let method: HTTPMethod
    public let host: URLHost
    public let path: URLPath
    public let scheme: URLScheme

    public let parameterType: ParameterEncodingType
    public let queryParameters: [URLQueryItem]?

    public let dateDecodingStrategy: JSONDecoder.DateDecodingStrategy?
}

/// Errors related to constructing a URL from a URLRequestable.
public enum URLConstructionError: Error {
    case invalidURL
}

/// Defines URL encoding types
public enum ParameterEncodingType {
    case queryString
    case formEncoded
}

/// Defines typed HTTP Methods.
public enum HTTPMethod: String {
    case GET, PUT, POST, DELETE
}

/// Defines typed URL Schemes
public enum URLScheme: String {
    case https
    case http
}

/// A typed URL host, allowing for cleaning syntax in constructing URLs.
public struct URLHost {

    public let value: String

    public static func named(_ name: String) -> URLHost {
        return URLHost(value: name)
    }
}

/// A typed URL path, allowing for cleaning syntax in constructing URLs.
public struct URLPath {
    public let value: String
    public static func path(_ path: String) -> URLPath {
        return URLPath(value: path)
    }
    public static func + (lhs: URLPath, rhs: URLPath) -> URLPath {
        return .path(lhs.value + rhs.value)
    }
}

public extension URLRequest {

    var typedMethod: HTTPMethod? {
        get { return httpMethod.flatMap { HTTPMethod(rawValue: $0) } }
        set { httpMethod = newValue?.rawValue }
    }

    static func `for`<T>(_ requestable: AnyRequestable<T>) throws -> URLRequest {

        guard let url = URLComponents.for(requestable).url else {
            throw URLConstructionError.invalidURL
        }

        var request = URLRequest(url: url)

        request.httpMethod = requestable.method.rawValue

        if
            .formEncoded == requestable.parameterType,
            let data = requestable.queryParameters?.formData {

            request.httpBody = data
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }

        return request
    }
}

public extension URLComponents {
    var typedScheme: URLScheme? {
        get { return scheme.flatMap { URLScheme(rawValue: $0) } }
        set { scheme = newValue?.rawValue }
    }
    var typedHost: URLHost? {
        get { return host.flatMap { URLHost(value: $0) } }
        set { host = newValue?.value }
    }
    var typedPath: URLPath {
        get { return URLPath(value: path) }
        set { path = newValue.value }
    }

    static func `for`<T>(_ requestable: AnyRequestable<T>) -> URLComponents {
        var comps = URLComponents()
        comps.typedHost = requestable.host
        comps.typedPath = requestable.path
        comps.typedScheme = requestable.scheme

        if .queryString == requestable.parameterType {
            comps.queryItems = requestable.queryParameters
        }

        return comps
    }
}

extension Array where Element == URLQueryItem {

    var formData: Data? {
        return encodedString?.data(using: .utf8)
    }

    var encodedString: String? {

        guard !isEmpty else { return nil }

        var returnString = ""

        for (index, item) in enumerated() {
            guard let value = item.value else { continue }
            if index != 0 {
                returnString.append("&")
            }
            returnString.append("\(item.name)=\(value)")
        }

        return returnString
    }
}
