//
//  StoryboardHelpers.swift
//  ARPNews
//
//  Created by Austin Younts on 11/4/17.
//  Copyright © 2017 ARPSynod. All rights reserved.
//

import UIKit

public protocol StoryboardType {
    func reference(in bundle: Bundle?) -> UIStoryboard
    var rawValue: String { get }
}

// Convenience methods for creating ViewControllers from Storyboards.
public extension StoryboardType {

    func reference(in bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: rawValue, bundle: bundle)
    }

    @available(iOS 13.0, *)
    func create<T: UIViewController>(identifier: String? = nil,
                                     creator: @escaping ((NSCoder) -> T)) -> T {
        let id = identifier ?? String(describing: T.self)
        let bundle = Bundle(for: T.self)
        return reference(in: bundle).instantiateViewController(identifier: id) { coder in
            return creator(coder)
        }
    }

    func create<T: UIViewController>(_ type: T.Type, identifier: String? = nil) -> T {
        let id = identifier ?? String(describing: type)
        let bundle = Bundle(for: T.self)
        guard let vc = reference(in: bundle).instantiateViewController(withIdentifier: id) as? T else {
            fatalError("Storyboard \(rawValue) does not container a ViewController of type \(type) for identifier \(id)")
        }
        return vc
    }

    func createInitial<T: UIViewController>(_ type: T.Type) -> T {
        let bundle = Bundle(for: T.self)
        guard let vc = reference(in: bundle).instantiateInitialViewController() as? T else {
            fatalError("Initial VC of \(rawValue) Storyboard is not of expected type: \(type)")
        }
        return vc
    }
}


public protocol StoryboardInjectable {

    associatedtype ViewControllerDependencies

    static func setDependencies(_ dependencies: ViewControllerDependencies)
    static func getDependencies() -> ViewControllerDependencies
}

public extension StoryboardInjectable {

    private static var storageKey: String {
        return String(describing: self)
    }

    private static var dependencyStorage: ViewControllerDependencies? {
        get { return NSObject.dependencyStorage[storageKey] as? ViewControllerDependencies }
        set { NSObject.dependencyStorage[storageKey] = newValue }
    }

    static func setDependencies(_ dependencies: ViewControllerDependencies) {
        dependencyStorage = dependencies
    }

    static func getDependencies() -> ViewControllerDependencies {
        guard let dependencies = dependencyStorage else { fatalError("No dependencies set") }
        dependencyStorage = nil
        return dependencies
    }
}

private extension NSObject {
    static var dependencyStorage: [String : Any] = [:]
}

class TestController: UIViewController, StoryboardInjectable {
    typealias ViewControllerDependencies = String
}

//==========================================================================
// MARK: - Segues
//==========================================================================

public extension UIStoryboardSegue {

    func destination<T: UIViewController>(as type: T.Type) -> T? {

        // If the destination is a UINavigationController, the caller is probably interested
        // in the type of the rootVC, rather than the nav.
        if let nav = destination as? UINavigationController,
            // Unless they are explicitly trying to cast as a UINavigationController.
            type != UINavigationController.self {
            return nav.viewControllers.first as? T
        }
        return destination as? T
    }
}

public extension UIViewController {

//    func handler() {
//        let segue = DefaultSegue(vc: TestController.self, deps: "")
//        perform(segue)
//    }

    func perform<T>(_ segue: DefaultSegue<T>) {
        segue.viewControllerType.setDependencies(segue.dependencies)
        performSegue(withIdentifier: segue.identifier, sender: self)
    }
}

public struct DefaultSegue<T: StoryboardInjectable>: SegueValues {

    public let dependencies: T.ViewControllerDependencies
    public let viewControllerType: T.Type
    public let identifier: String

    public init(vc: T.Type, dependencies: T.ViewControllerDependencies, identifier: String? = nil) {
        self.dependencies = dependencies
        self.viewControllerType = vc
        self.identifier = identifier ?? String(describing: vc)
    }

    public static func segue(to vc: T.Type,
                      with deps: T.ViewControllerDependencies,
                      identifier: String? = nil) -> DefaultSegue<T> {
        return DefaultSegue(vc: vc, dependencies: deps, identifier: identifier)
    }
}

public protocol SegueValues {

    associatedtype ViewController
    associatedtype Dependencies

    var dependencies: Dependencies { get }
    var viewControllerType: ViewController.Type { get }
    var identifier: String { get }
}
