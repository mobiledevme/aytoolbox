//
//  ThreadSafeArray.swift
//  Created by Austin Younts on 6/11/17.
//
import Foundation

class ThreadSafeArray<T> {

    typealias Element = T

    fileprivate var wrappedArray = [T]()
    fileprivate let queue = DispatchQueue(label: "ArrayQueue_\(arc4random_uniform(100000))")

    init(array: [T]) {
        wrappedArray = array
    }

    var count: Int {
        get {
            return wrappedArray.count
        }
    }

    func allItems() -> [T] {
        return wrappedArray
    }

    func append(_ item: T) {
        queue.sync { self._append(item) }
    }

    func addItems(_ items: [T]) {
        queue.sync { self._addItems(items) }
    }

    func getToIndex(_ index: Int) -> [T] {

        var items = [T]()

        queue.sync {
            items = self._getToIndex(index)
        }

        return items
    }

    func removeToIndex(_ index: Int) -> [T] {

        var itemsToRemove = [T]()

        queue.sync {
            itemsToRemove = self._removeToIndex(index)
        }

        return itemsToRemove
    }

    //==========================================================================
    // MARK: - Implementation
    //==========================================================================


    fileprivate var _count: Int {
        get {
            return wrappedArray.count
        }
    }

    fileprivate func _allItems() -> [T] {
        return wrappedArray
    }

    fileprivate func _append(_ item: T) {
        self.wrappedArray.append(item)
    }

    fileprivate func _addItems(_ items: [T]) {
        self.wrappedArray += items
    }

    fileprivate func _getToIndex(_ index: Int) -> [T] {

        let range = 0..<index
        let items = Array(self.wrappedArray[range])

        return items
    }

    fileprivate func _removeToIndex(_ index: Int) -> [T] {

        let range = 0..<index
        let itemsToRemove = Array(self.wrappedArray[range])
        self.wrappedArray.replaceSubrange(range, with: [T]())

        return itemsToRemove
    }
}

extension ThreadSafeArray where ThreadSafeArray.Element: Equatable {

    func index(of element: Element) -> Int? {
        for (index, item) in wrappedArray.enumerated() {
            if item == element {
                return index
            }
        }
        return nil
    }

    func replace(_ element: Element) {
        queue.sync {
            guard let index = index(of: element) else { return }
            wrappedArray[index] = element
        }
    }

    func filter(_ isIncluded: (Element) -> Bool) -> [Element] {
        var filtered: [Element] = []
        queue.sync {
            filtered = wrappedArray.filter({ isIncluded($0) })
        }
        return filtered
    }
}

