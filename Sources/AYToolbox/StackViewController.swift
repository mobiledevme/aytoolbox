//
//  StackViewController.swift
//  AYUtilities
//
//  Created by Austin on 2/10/18.
//  Copyright © 2018 ARY. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
public class StackViewController: UIViewController {

    private let _rootStackView: UIStackView = {
        return UIStackView().useAutolayout()
    }()

    public var rootStackView: UIStackView {
        return _rootStackView
    }

    public override func loadView() {
        view = rootStackView
    }
}
