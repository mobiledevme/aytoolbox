//
//  OperationHelpers.swift
//  AYUtilities
//
//  Created by Austin Younts on 3/18/18.
//  Copyright © 2018 ARY. All rights reserved.
//

import Foundation

public extension OperationQueue {

    convenience init(maxConcurrentCount: Int, qualityOfService quality: QualityOfService = .default) {
        self.init()
        maxConcurrentOperationCount = maxConcurrentCount
        qualityOfService = quality
    }
}
