//
//  ObserverSet.swift
//  ObserverSet
//
//  Created by Mike Ash on 1/22/15.
//  Copyright (c) 2015 Mike Ash. All rights reserved.
//

import Foundation

public typealias ObserverAction<Parameters> = (AnyObject) -> (Parameters) -> Void


public class ObserverSetEntry<Parameters> {
    fileprivate weak var object: AnyObject?
    fileprivate let f: ObserverAction<Parameters>

    fileprivate init(object: AnyObject, f: @escaping ObserverAction<Parameters>) {
        self.object = object
        self.f = f
    }
}

public class ObserverSet<Parameters>: CustomStringConvertible {
    // Locking support

    private var queue = DispatchQueue(label: "com.mvz3.ObserverSet")

    private func synchronized(f: (() -> Void)) {
        queue.sync(execute: f)
    }


    // Main implementation

    private var entries: [ObserverSetEntry<Parameters>] = []

    public init() {}

    public func add<T: AnyObject>(object: T, _ f: @escaping ObserverAction<Parameters>) -> ObserverSetEntry<Parameters> {
        let entry = ObserverSetEntry<Parameters>(object: object, f: { f($0 as! T) })
        synchronized {
            self.entries.append(entry)
        }
        return entry
    }

    public func add(_ f: @escaping (Parameters) -> Void) -> ObserverSetEntry<Parameters> {
        return self.add(object: self, { ignored in f })
    }

    public func remove(entry: ObserverSetEntry<Parameters>) {
        synchronized {
            self.entries = self.entries.filter{ $0 !== entry }
        }
    }

    public func notify(parameters: Parameters) {
        var toCall: [(Parameters) -> Void] = []

        synchronized {
            for entry in self.entries {
                if let object: AnyObject = entry.object {
                    toCall.append(entry.f(object))
                }
            }
            self.entries = self.entries.filter{ $0.object != nil }
        }

        for f in toCall {
            f(parameters)
        }
    }


    // MARK: CustomStringConvertible

    public var description: String {
        var entries: [ObserverSetEntry<Parameters>] = []
        synchronized {
            entries = self.entries
        }

        let strings = entries.map{
            entry in
            (entry.object === self
                ? "\(String(describing: entry.f))"
                : "\(String(describing: entry.object)) \(String(describing: entry.f))")
        }
        let joined = strings.joined(separator: ", ")

        return "\(Mirror(reflecting: self).description): (\(joined))"
    }
}


