import XCTest

import AYToolboxTests

var tests = [XCTestCaseEntry]()
tests += AYToolboxTests.allTests()
XCTMain(tests)
